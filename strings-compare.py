import os
import re
import argparse
import subprocess


class _Constants:

    def __init__(self):
        pass

    @staticmethod
    def disallowed_regexp():
        return "[\s\!\@#\%\^&\*\(\)=]"


class _ObjectWithDictionary:

    def __init__(self, dict):
        self.__dict__.update(dict)


class _Configuration:

    @classmethod
    def default(cls):
        path = subprocess.check_output("pwd")
        args = _ObjectWithDictionary({"projectpath": path,
                                      "filename": "Localizable.strings",
                                      "checkuntranslated": False,
                                      "base": "Base.lproj",
                                      "validateregexp": _Constants.disallowed_regexp()})

        return _Configuration(args)

    def __init__(self, args):
        self.project_path = args.projectpath
        self.strings_filename = args.filename or "Localizable.strings"
        self.should_check_untranslated_keys = args.checkuntranslated
        self.base_directory = args.base or "Base.lproj"
        self.disallowed_regexp = args.validateregexp or _Constants.disallowed_regexp()


class _ArgManager:

    def __init__(self):
        arg_parser = argparse.ArgumentParser()
        arg_parser.add_argument("projectpath",
                                type=str,
                                help="Path to the project directory containing .lproj directories")
        arg_parser.add_argument("-u", "--checkuntranslated",
                                help="Provides nominal checking for untranslated keys by comparing values",
                                action="store_true")
        arg_parser.add_argument("-f", "--filename",
                                type=str,
                                help="Common strings filename - must be the same in all .lproj containers, defaults to 'Localizable.strings'")
        arg_parser.add_argument("-b", "--base",
                                type=str,
                                help="Directory name of the base '.lproj'. This will be the strings file to use as the basis for comparisons. Defaults to 'Base.lproj'")
        arg_parser.add_argument("-v", "--validateregexp",
                                type=str,
                                help="Regular expression to use to validate keys in the strings file")

        self.parsed_args = arg_parser.parse_args()


class _StringsFile:

    def __init__(self, filepath):
        self.file = open(filepath, "rb")
        self.as_dictionary = self.__as_dictionary()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()

    @staticmethod
    def __clean_line(line):
        return line.strip()

    def __as_dictionary(self):
        raw_lines = self.file.readlines()
        lines = []
        for line in raw_lines:
            if not line.startswith("//") and not line.startswith("\n"):
                lines.append(line)

        lines_dict = {}
        for line in lines:
            line_array = line.split("=")
            if len(line_array) == 2:
                key = self.__clean_line(line_array[0])
                value = self.__clean_line(line_array[1])
                lines_dict[key] = value

        return lines_dict


class StringsFileChecker:

    def __init__(self, configuration):
        self.configuration = configuration

    @staticmethod
    def __strings_from_file_at_path(path):
        return _StringsFile(path)

    @staticmethod
    def __compare_keys_in_dictionaries(base_dictionary, dictionary):
        missing_keys = []
        for key in base_dictionary:
            if key not in dictionary:
                missing_keys.append(key)

        return missing_keys

    @staticmethod
    def __compare_values_in_dictionaries(base_dictionary, dictionary):
        matching_values = []
        for key in base_dictionary:
            if key in dictionary and dictionary[key] == base_dictionary[key]:
                matching_values.append("%s: %s" % (key, dictionary[key]))

        return matching_values

    @staticmethod
    def __validate_keys_in_dictionary(dictionary, disallowed_chars_regexp):
        invalid_keys = []
        for key in dictionary:
            match = re.search(disallowed_chars_regexp, key)
            if match:
                invalid_keys.append(key)

        return invalid_keys

    @staticmethod
    def __output_result(result, success_message, failure_message):
        if len(result) > 0:
            message = "%s\n\t%s" % (failure_message, "\n\t".join(result))
        else:
            message = "%s\n" % success_message
        print("\n%s" % message)

    def compare(self):
        base_path = os.path.join(self.configuration.project_path, self.configuration.base_directory)
        full_base_path = os.path.join(base_path, self.configuration.strings_filename)
        base_strings_dict = self.__strings_from_file_at_path(full_base_path).as_dictionary

        print("\nChecking all '%s' files in '%s'...\n" %
              (self.configuration.strings_filename, self.configuration.project_path))

        for directory in os.listdir(self.configuration.project_path):
            if directory.endswith("lproj") and directory != self.configuration.base_directory:
                path = os.path.join(self.configuration.project_path, directory)
                full_path = os.path.join(path, self.configuration.strings_filename)
                strings_dict = self.__strings_from_file_at_path(full_path).as_dictionary

                print("------------------------------------------")
                print("Inspecting %s...\n" % full_path)

                comparison_result = self.__compare_keys_in_dictionaries(base_strings_dict,
                                                                        strings_dict)
                self.__output_result(comparison_result,
                                     "No missing keys :)",
                                     "Missing keys:")

                if self.configuration.should_check_untranslated_keys:
                    comparison_result = self.__compare_values_in_dictionaries(base_strings_dict,
                                                                              strings_dict)
                    self.__output_result(comparison_result,
                                         "No untranslated keys :)",
                                         "Untranslated keys (with values):")

                validation_result = self.__validate_keys_in_dictionary(strings_dict,
                                                                       self.configuration.disallowed_regexp)
                self.__output_result(validation_result,
                                     "All Keys Valid :)",
                                     "Invalid keys:")


StringsFileChecker(_Configuration(_ArgManager().parsed_args)).compare()
