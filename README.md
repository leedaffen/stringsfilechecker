# iOS Localizable.strings Checker

Compares the keys in the Base.lproj Localizable.strings file with those in all other .lproj directories.  

## Usage
```
strings-compare.py [-h] [-u] [-f FILENAME] [-b BASE]
                          [-v VALIDATEREGEXP]
                          projectpath

positional arguments:
  projectpath           Path to the project directory containing .lproj
                        directories

optional arguments:
  -h, --help            show this help message and exit
  -u, --checkuntranslated
                        Provides nominal checking for untranslated keys by
                        comparing values
  -f FILENAME, --filename FILENAME
                        Common strings filename - must be the same in all
                        .lproj containers, defaults to 'Localizable.strings'
  -b BASE, --base BASE  Directory name of the base '.lproj'. This will be the
                        strings file to use as the basis for comparisons.
                        Defaults to 'Base.lproj'
  -v VALIDATEREGEXP, --validateregexp VALIDATEREGEXP
                        Regular expression to use to validate keys in the
                        strings file
```
